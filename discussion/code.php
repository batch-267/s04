<?php

class Building{

	//Access Modifiers
	//These are keywords that can be used to control the "visibility" of properties and methods in a class

	//public: fully open, properties and methods can be accessed from everywhere

	//private: method and properties can only be accessed within the class and disables inheritance

	//protected: property or method is only accessible within the class and its child class
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
	public function getName(){
		return $this->name;
	}
	public function getFloors(){
		return $this->floors;
	}
	public function getAddress(){
		return $this->address;
	}
	// setter - is used to change/modify the default value of a property of an instantiated object
	public function setName($name){
		//$this->name = $name;
		if(gettype($name)==="string"){
			$this->name = $name;
		}
	}

	private function setFloors($floors){
		$this->floors = $floors;
	}

	private function setAddress($address){
		$this->address = $address;		
	}
}

class Condominium extends Building{

	// "Encapsulation" indicates that data must not be directly accessible to users, but can be access only through public functions (setter and getter function)

	// Getters and Setters
		// This are used to retrieve and modify values of each property of the object.
		// Each property of an object should have a set of getter and setter function.


	//getter- is used to retrieve/access the value of an instantiated object

	//not necessary to declare because of inheritance

	// public function getName(){
	// 	return $this->name;
	// }
	
	// public function getFloors(){
	// 	return $this->floors;
	// }
	// public function getAddress(){
	// 	return $this->address;
	// }
	// setter - is used to change/modify the default value of a property of an instantiated object
	// public function setName($name){
	// 	//$this->name = $name;
	// 	if(gettype($name)==="string"){
	// 		$this->name = $name;
	// 	}
	// }

	// private function setFloors($floors){
	// 	$this->floors = $floors;
	// }

	// private function setAddress($address){
	// 	$this->address = $address;		
	// }

}


$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");


$condominium = new Condominium("Enzo Condo",5,"Buendia Avenue, Makati City, Philippines");


